package fpd.dialogs;

import fpd.FPD;
import fpd.KeyListener;
import fpd.TextEditor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

public class FindDialog extends JDialog {

	private final TextEditor textEditor;
	private String textToFind;

	public FindDialog(FPD fpd, TextEditor textEditor) {
		super(fpd, true);
		this.textEditor = textEditor;
		setTitle("Find text");
		setLayout(new GridBagLayout());
		setResizable(false);
		setSize(400, 100);
		setLocationRelativeTo(fpd);
		add(new JLabel("Text to find"), constraints(0, 0, 0.3));
		add(textToFind(), constraints(1, 0, 0.7));
		add(toolbar(), toolbarConstraints());
		setVisible(true);
	}

	@Override
	protected JRootPane createRootPane() {
		JRootPane pane = super.createRootPane();
		pane.registerKeyboardAction(e -> findAction(), KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
		pane.registerKeyboardAction(e -> this.dispose(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
		return pane;
	}

	private GridBagConstraints toolbarConstraints() {
		return new GridBagConstraints(0, 1, 2, 1, 1, 0, GridBagConstraints.LINE_END, GridBagConstraints.HORIZONTAL, new Insets(0, 10, 0, 0), 0, 0);
	}

	private Component toolbar() {
		JPanel component = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		component.add(findButton());
		component.add(closeButton());
		return component;
	}

	private Component closeButton() {
		JButton button = new JButton("Close");
		button.addActionListener(e -> this.dispose());
		return button;
	}


	private Component findButton() {
		JButton button = new JButton("Find next");
		button.addActionListener(e -> findAction());
		return button;
	}

	private void findAction() {
		getComponent(getComponentCount() - 1).requestFocusInWindow();
		int index = textEditor.highlight(textToFind);
		if (index == -1) JOptionPane.showMessageDialog(this, "Text not found");
		dispose();
	}

	private GridBagConstraints constraints(int gridX, int gridY, double weightX) {
		return new GridBagConstraints(gridX, gridY, 1, 1, weightX, 0, GridBagConstraints.LINE_START, GridBagConstraints.HORIZONTAL, new Insets(5, 10, 0, 10), 0, 0);
	}

	private Component textToFind() {
		JTextField component = new JTextField();
		component.addKeyListener(new KeyListener(t -> textToFind = t));
		return component;
	}

}
