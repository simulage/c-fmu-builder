package fpd.dialogs;

import fpd.KeyListener;
import fpd.Variables;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;

public class InsertVariableDialog extends JDialog {

	private String name = "var1";
	private String type = "Real";
	private String causality = "input";
	private String variability = "discrete";
	private String initial = "exact";
	private String startValue = "";
	private boolean invalid;

	public InsertVariableDialog(JFrame frame) {
		super(frame, true);
		setTitle("Insert variable");
		setLayout(new GridBagLayout());
		setResizable(false);
		setSize(400, 250);
		setLocationRelativeTo(frame);
		add(label("Name"), constraints(0, 0, 0.3));
		add(nameText(), constraints(1, 0, 0.7));
		add(label("Type"), constraints(0, 1, 0.3));
		add(typeCombo(), constraints(1, 1, 0.7));
		add(label("Causality"), constraints(0, 2, 0.3));
		add(causalityCombo(), constraints(1, 2, 0.7));
		add(label("Variability"), constraints(0, 3, 0.3));
		add(variabilityCombo(), constraints(1, 3, 0.7));
		add(label("Initial"), constraints(0, 4, 0.3));
		add(initialCombo(), constraints(1, 4, 0.7));
		add(label("Start value"), constraints(0, 5, 0.3));
		add(startValueText(), constraints(1, 5, 0.7));
		add(toolbar(), toolbarConstraints());
	}

	@Override
	protected JRootPane createRootPane() {
		JRootPane pane = super.createRootPane();
		pane.registerKeyboardAction(e -> insertAction(), KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
		pane.registerKeyboardAction(e -> cancelAction(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
		return pane;
	}

	private GridBagConstraints toolbarConstraints() {
		return new GridBagConstraints(0, 6, 2, 1, 1, 0, GridBagConstraints.LINE_END, GridBagConstraints.HORIZONTAL, new Insets(0, 10, 0, 0), 0, 0);
	}

	private Component toolbar() {
		JPanel component = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		component.add(acceptButton());
		component.add(cancelButton());
		return component;
	}

	private Component acceptButton() {
		JButton button = new JButton("Insert");
		button.addActionListener(e -> insertAction());
		return button;
	}

	private void insertAction() {
		this.getContentPane().transferFocus();
		this.dispose();
	}

	private Component cancelButton() {
		JButton button = new JButton("Cancel");
		button.addActionListener(e -> cancelAction());
		return button;
	}

	private void cancelAction() {
		invalid = true;
		InsertVariableDialog.this.dispose();
	}

	private GridBagConstraints constraints(int gridX, int gridY, double weightX) {
		return new GridBagConstraints(gridX, gridY, 1, 1, weightX, 0, GridBagConstraints.LINE_START, GridBagConstraints.HORIZONTAL, new Insets(5, 10, 0, 10), 0, 0);
	}

	private Component typeCombo() {
		JComboBox<String> component = new JComboBox<>(Variables.TYPES);
		component.addItemListener(e -> type = e.getStateChange() != ItemEvent.SELECTED ? type : e.getItem().toString());
		return component;
	}

	private Component causalityCombo() {
		JComboBox<String> component = new JComboBox<>(Variables.CAUSALITIES);
		component.addItemListener(e -> causality = e.getStateChange() != ItemEvent.SELECTED ? type : e.getItem().toString());
		return component;
	}

	private Component variabilityCombo() {
		JComboBox<String> component = new JComboBox<>(Variables.VARIABILITIES);
		component.addItemListener(e -> variability = e.getStateChange() != ItemEvent.SELECTED ? type : e.getItem().toString());
		return component;
	}

	private Component initialCombo() {
		JComboBox<String> component = new JComboBox<>(Variables.INITIALS);
		component.addItemListener(e -> initial = e.getStateChange() != ItemEvent.SELECTED ? type : e.getItem().toString());
		return component;
	}

	private Component label(String label) {
		return new JLabel(label);
	}

	private Component nameText() {
		JTextField component = new JTextField(name);
		component.addKeyListener(new KeyListener(t -> name = t));
		return component;
	}

	private Component startValueText() {
		JTextField component = new JTextField();
		component.addKeyListener(new KeyListener(t -> startValue = t));
		return component;
	}

	public String launch() {
		setVisible(true);
		return invalid ? "" : toString();
	}

	@Override
	public String toString() {
		return "[Variable]" +
				"\nname: " + name +
				"\ntype: " + type +
				"\ncausality: " + causality +
				"\nvariability: " + variability +
				"\ninitial: " + initial +
				(startValue.isEmpty() ? "" : "\nstartValue: " + startValue) +
				"\n\n";
	}
}
