package fpd;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

import static java.awt.event.KeyEvent.*;
import static javax.swing.KeyStroke.getKeyStroke;

class Menu {
	private static final int Mask = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();
	private FPD FPD;

	static JMenuBar of(FPD FPD) {
		return new Menu(FPD).create();
	}

	Menu(FPD FPD) {
		this.FPD = FPD;
	}

	JMenuBar create() {
		JMenuBar menubar = new JMenuBar();
		menubar.add(fileMenu());
		menubar.add(editMenu());
		menubar.add(codeMenu());
		menubar.add(projectMenu());
		menubar.add(helpMenu());
		return menubar;
	}

	private JMenu fileMenu() {
		JMenu menu = new JMenu("File");
		menu.add(newMenuItem());
		menu.add(openMenuItem());
		menu.add(saveMenuItem());
//		menu.add(importMenuItem());
		menu.addSeparator();
		menu.add(openWorkspace());
		menu.add(setWorkspace());
		menu.add(exitMenuItem());
		return menu;
	}

	private JMenu editMenu() {
		JMenu menu = new JMenu("Edit");
		menu.add(undoMenuItem());
		menu.add(redoMenuItem());
		menu.addSeparator();
		menu.add(cutMenuItem());
		menu.add(copyMenuItem());
		menu.add(pasteMenuItem());
		menu.addSeparator();
		menu.add(findMenuItem());
		menu.add(selectAllMenuItem());
		return menu;
	}

	private JMenu codeMenu() {
		JMenu menu = new JMenu("Code");
		menu.add(checkMenuItem());
		menu.addSeparator();
		menu.add(insertVariableMenuItem());
		return menu;
	}

	private JMenu projectMenu() {
		JMenu menu = new JMenu("Tools");
		menu.add(generateMenuItem());
		menu.add(openProjectMenuItem());
		menu.add(buildMenuItem());
		return menu;
	}

	private JMenu helpMenu() {
		JMenu menu = new JMenu("Help");
		return menu;
	}

	private JMenuItem newMenuItem() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.New));
		menuItem.setAccelerator(getKeyStroke(VK_N, Mask));
		return menuItem;
	}

	private JMenuItem openMenuItem() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.Open));
		menuItem.setAccelerator(getKeyStroke(VK_O, Mask));
		return menuItem;
	}

	private JMenuItem saveMenuItem() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.Save));
		menuItem.setAccelerator(getKeyStroke(VK_S, Mask));
		return menuItem;
	}

	private JMenuItem importMenuItem() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.Import));
		return menuItem;
	}

	private JMenuItem generateMenuItem() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.Generate));
		menuItem.setAccelerator(getKeyStroke(VK_G, Mask));
		return menuItem;
	}

	private JMenuItem openProjectMenuItem() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.OpenProject));
		menuItem.setAccelerator(getKeyStroke(VK_L, Mask));
		return menuItem;
	}

	private JMenuItem buildMenuItem() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.Build));
		menuItem.setAccelerator(getKeyStroke(VK_B, Mask));
		return menuItem;
	}

	private JMenuItem openWorkspace() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.OpenWorkspace));
		return menuItem;
	}

	private JMenuItem setWorkspace() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.SetWorkspace));
		return menuItem;
	}

	private JMenuItem exitMenuItem() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.Exit));
		menuItem.setAccelerator(getKeyStroke(VK_Q, Mask));
		return menuItem;
	}

	private JMenuItem undoMenuItem() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.Undo));
		menuItem.setAccelerator(getKeyStroke(VK_Z, Mask));
		return menuItem;
	}

	private JMenuItem redoMenuItem() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.Redo));
		menuItem.setAccelerator(getKeyStroke(VK_Y, Mask));
		return menuItem;
	}

	private JMenuItem cutMenuItem() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.Cut));
		menuItem.setAccelerator(getKeyStroke(VK_X, Mask));
		return menuItem;
	}

	private JMenuItem copyMenuItem() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.Copy));
		menuItem.setAccelerator(getKeyStroke(VK_C, Mask));
		return menuItem;
	}

	private JMenuItem pasteMenuItem() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.Paste));
		menuItem.setAccelerator(getKeyStroke(VK_P, Mask));
		return menuItem;
	}

	private JMenuItem findMenuItem() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.Find));
		menuItem.setAccelerator(getKeyStroke(VK_F, Mask));
		return menuItem;
	}

	private JMenuItem selectAllMenuItem() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.SelectAll));
		menuItem.setAccelerator(getKeyStroke(VK_A, Mask));
		return menuItem;
	}

	private JMenuItem checkMenuItem() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.Check));
		menuItem.setAccelerator(getKeyStroke(KeyEvent.VK_1, Mask));
		return menuItem;
	}

	private JMenuItem insertVariableMenuItem() {
		JMenuItem menuItem = new JMenuItem(FPD.get(fpd.FPD.ActionId.InsertVariable));
		menuItem.setAccelerator(getKeyStroke(KeyEvent.VK_I, Mask));
		return menuItem;
	}
}
