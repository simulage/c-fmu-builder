package fpd;

import fpd.dialogs.FindDialog;
import fpd.dialogs.InsertVariableDialog;
import io.intino.alexandria.inl.InlReader;
import io.intino.alexandria.inl.Message;
import sun.misc.IOUtils;

import javax.swing.*;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.DefaultEditorKit;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static fpd.FPD.ActionId.*;
import static java.util.Arrays.stream;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;

public class FPD extends JFrame {
	public static final String FPD = ".fpd";
	private static final Dimension size = new Dimension(960, 600);
	private final Map<ActionId, Action> actions;
	private final TextEditor textEditor;
	private File workspace = new File("workspace");
	private UndoManager undoManager;

	private FPD() {
		this.textEditor = emptyTextEditor();
		this.textEditor.getDocument().addUndoableEditListener(undoableEditListener());
		this.actions = actions();
		this.setTitle("FMU Project Designer");
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.setSize(size);
		this.setMinimumSize(size);
		this.setLocationRelativeTo(null);
		this.setJMenuBar(Menu.of(this));
		this.setLayout(new GridBagLayout());
		this.getContentPane().add(new JScrollPane(textEditor), constraints(0, 1));
	}

	public static void main(String[] args) {
		Locale.setDefault(Locale.ENGLISH);
		SwingUtilities.invokeLater(() -> new FPD().launch());
	}

	private UndoableEditListener undoableEditListener() {
		this.undoManager = new UndoManager();
		return new UndoableEditListener() {
			@Override
			public void undoableEditHappened(UndoableEditEvent e) {
				undoManager.undoableEditHappened(e);
				update(get(Undo), undoManager.canUndo());
				update(get(Redo), undoManager.canRedo());
			}

			private void update(Action action, boolean value) {
				action.setEnabled(value);
			}
		};
	}

	private TextEditor emptyTextEditor() {
		try {
			return new TextEditor();
		} catch (IOException ignored) {
			return null;
		}
	}

	private Map<ActionId, Action> actions() {
		Map<ActionId, Action> actions = new HashMap<>();
		actions.put(New, newAction());
		actions.put(Open, openAction());
		actions.put(Import, importAction());
		actions.put(Save, saveAction());
		actions.put(OpenWorkspace, openWorkspaceAction());
		actions.put(SetWorkspace, setWorkspaceAction());
		actions.put(Exit, exitAction());
		actions.put(Undo, undoAction());
		actions.put(Redo, redoAction());
		actions.put(Cut, cutAction());
		actions.put(Copy, copyAction());
		actions.put(Paste, pasteAction());
		actions.put(Find, findAction());
		actions.put(SelectAll, selectAllAction());
		actions.put(Check, checkAction());
		actions.put(InsertVariable, insertVariableAction());
		actions.put(Generate, generateAction());
		actions.put(OpenProject, openProjectAction());
		actions.put(Build, buildAction());
		return actions;
	}

	private GridBagConstraints constraints(int gridx, double weightx) {
		return new GridBagConstraints(gridx, 0, 1, 1, weightx, 1, GridBagConstraints.LINE_START, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0);
	}

	private Component workSpacePanel() {
		JPanel component = new JPanel();
		component.setBackground(new Color(0x3c, 0x3f, 0x41));
		component.setSize(300, 100);
		return component;
	}

	private void launch() {
		this.setVisible(true);
	}

	private Action newAction() {
		return new AbstractAction("New") {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (textEditor.isModified() && !isSaved()) return;
				try {
					textEditor.setFile(null);
				} catch (IOException ignored) {
				}
			}
		};
	}

	private Action openAction() {
		return new AbstractAction("Open") {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (textEditor.isModified() && !isSaved()) return;
				try {
					File file = open();
					if (file != null) textEditor.setFile(file);
				} catch (IOException ignored) {
				}
			}
		};
	}

	private Action importAction() {
		return new AbstractAction("Import...") {
			@Override
			public void actionPerformed(ActionEvent e) {
				//TODO later
			}
		};
	}

	private Action saveAction() {
		return new AbstractAction("Save") {
			@Override
			public void actionPerformed(ActionEvent e) {
				save();
			}
		};
	}

	private Action openWorkspaceAction() {
		return new AbstractAction("Open Workspace") {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Desktop.getDesktop().open(workspace);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		};
	}

	private Action setWorkspaceAction() {
		return new AbstractAction("Set Workspace...") {
			@Override
			public void actionPerformed(ActionEvent e) {
				File file = openFolder();
				workspace = file != null ? file : workspace;
				workspace.mkdirs();
			}
		};
	}

	private Action exitAction() {
		return new AbstractAction("Exit") {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (textEditor.isModified() && !isSaved()) return;
				System.exit(0);
			}
		};
	}

	private Action undoAction() {
		return new AbstractAction("Undo") {
			@Override
			public void actionPerformed(ActionEvent e) {
				undoManager.undo();
				get(Redo).setEnabled(true);
				if (!undoManager.canUndo()) setEnabled(false);
			}
		};
	}

	private Action redoAction() {
		return new AbstractAction("Redo") {
			@Override
			public void actionPerformed(ActionEvent e) {
				undoManager.redo();
				get(Undo).setEnabled(true);
				if (!undoManager.canRedo()) setEnabled(false);
			}
		};
	}

	private Action cutAction() {
		Action action = new DefaultEditorKit.CutAction();
		action.putValue(Action.NAME, "Cut");
		return action;
	}

	private Action copyAction() {
		Action action = new DefaultEditorKit.CopyAction();
		action.putValue(Action.NAME, "Copy");
		return action;
	}

	private Action pasteAction() {
		Action action = new DefaultEditorKit.PasteAction();
		action.putValue(Action.NAME, "Paste");
		return action;
	}

	private Action findAction() {
		return new AbstractAction("Find...") {
			@Override
			public void actionPerformed(ActionEvent e) {
				new FindDialog(FPD.this, textEditor);
			}
		};
	}

	private Action selectAllAction() {
		return new AbstractAction("Select all") {
			@Override
			public void actionPerformed(ActionEvent e) {
				textEditor.selectAll();
			}
		};
	}

	private Action checkAction() {
		return new AbstractAction("Check") {
			@Override
			public void actionPerformed(ActionEvent e) {
				doCheck(true);
			}
		};
	}

	private boolean doCheck(boolean showOk) {
		Error error = new Checker().check(textEditor.getText());
		if (error != null) {
			textEditor.markAsError(error.lineIndex());
			JOptionPane.showMessageDialog(FPD.this, error.error(), "Error", ERROR_MESSAGE);
			return false;
		} else if (showOk) {
			JOptionPane.showMessageDialog(FPD.this, "Definition checked successfully", "Check", INFORMATION_MESSAGE);
			return true;
		}
		return true;
	}

	private Action insertVariableAction() {
		return new AbstractAction("Insert variable...") {

			private InsertVariableDialog dialog;

			@Override
			public void actionPerformed(ActionEvent e) {
				dialog = new InsertVariableDialog(FPD.this);
				textEditor.append(dialog.launch());
			}
		};
	}

	private Action generateAction() {
		return new AbstractAction("Generate project") {

			@Override
			public void actionPerformed(ActionEvent e) {
				InlReader reader = new InlReader(new ByteArrayInputStream(textEditor.getText().getBytes()));
				Message fmu = reader.hasNext() ? reader.next() : null;
				if (fmu == null) return;
				File projectFolder = new File(workspace, fmu.get("Name"));
				stream(new String[]{"resources", "src", "test"}).forEach(s -> new File(projectFolder, s).mkdirs());
				List<Message> variables = new ArrayList<>();
				while (reader.hasNext()) variables.add(reader.next());
				new XmlCreator(fmu, variables).create(projectFolder);
				try {
					Files.write(new File(projectFolder, fmu.get("name") + ".fpd").toPath(), textEditor.getText().getBytes());
					Desktop.getDesktop().open(projectFolder);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		};
	}

	private Action openProjectAction() {
		return new AbstractAction("Open project") {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!doCheck(false)) return;
				try {
					Desktop.getDesktop().open(new File(workspace, fmuMessage().get("name")));
				} catch (Exception ignored) {
					JOptionPane.showMessageDialog(FPD.this, "Generate project first");
				}
			}
		};
	}

	private Action buildAction() {
		return new AbstractAction("Build FMU...") {

			@Override
			public void actionPerformed(ActionEvent e) {
				Message fmu = fmuMessage();
				File projectFolder = new File(workspace, fmu.get("name"));
				try {
					File fmuFile = new File(projectFolder, fmu.get("name") + ".fmu");
					ZipOutputStream stream = new ZipOutputStream(new FileOutputStream(fmuFile));
					stream.putNextEntry(new ZipEntry("modelDescription.xml"));
					stream.write(Files.readAllBytes(new File(projectFolder, "modelDescription.xml").toPath()));
					stream.closeEntry();
					stream.putNextEntry(new ZipEntry("binaries/" + fmu.get("name") + ".dylib"));
					stream.closeEntry();
					stream.close();
					Desktop.getDesktop().open(fmuFile.getParentFile());
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		};
	}

	private Message fmuMessage() {
		InlReader reader = new InlReader(new ByteArrayInputStream(textEditor.getText().getBytes()));
		return reader.hasNext() ? reader.next() : null;
	}

	private boolean isSaved() {
		int result = JOptionPane.showConfirmDialog(this, "Do you want to save current project?", "", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, questionIcon());
		if (result == JOptionPane.CANCEL_OPTION) return false;
		if (result == JOptionPane.NO_OPTION) return true;
		return save();
	}

	private Icon questionIcon() {
		return new ImageIcon(imageOf("question.png"));
	}

	private byte[] imageOf(String name) {
		try {
			return IOUtils.readFully(FPD.class.getResourceAsStream("/images/" + name), -1, false);
		} catch (IOException e) {
			e.printStackTrace();
			return new byte[0];
		}
	}

	private File open() {
		JFileChooser chooser = new JFileChooser(workspace);
		chooser.setFileFilter(new FileNameExtensionFilter("FMU Project file (.fpd)", FPD.substring(1)));
		int result = chooser.showOpenDialog(this);
		return result != JFileChooser.CANCEL_OPTION ? chooser.getSelectedFile() : null;
	}

	private File openFolder() {
		JFileChooser chooser = new JFileChooser(workspace);
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);
		int result = chooser.showOpenDialog(this);
		return result != JFileChooser.CANCEL_OPTION ? chooser.getSelectedFile() : null;
	}

	private boolean save() {
		if (!doCheck(false)) return false;
		Message fmu = fmuMessage();
		File projectFolder = new File(workspace, fmu.get("name"));
		projectFolder.mkdirs();
		textEditor.changeLocation(new File(projectFolder, fmu.get("name") + ".fpd"));
		try {
			textEditor.save();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public Action get(ActionId actionId) {
		return actions.get(actionId);
	}


	enum ActionId {
		New, Open, Import, Save, OpenWorkspace, SetWorkspace, Exit, Undo, Redo, Cut, Copy, Paste, SelectAll, Find, Check, InsertVariable, Generate, OpenProject, Build
	}

}
