package fpd;

import static java.lang.Boolean.parseBoolean;
import static java.lang.Double.parseDouble;
import static java.util.Arrays.asList;
import static javax.xml.bind.DatatypeConverter.parseInteger;

public class Variables {
	public static final String[] INITIALS = {"exact", "approx", "calculated"};
	public static final String[] VARIABILITIES = {"constant", "fixed", "tunable", "discrete", "continuous"};
	public static final String[] CAUSALITIES = {"parameter", "calculatedParameter", "input", "output", "local", "independent"};
	public static final String[] TYPES = {"Real", "Integer", "Boolean", "String"};

	static boolean typeExists(String typeValue) {
		return asList(TYPES).contains(typeValue);
	}

	static boolean causalityExists(String causality) {
		return asList(CAUSALITIES).contains(causality);
	}

	static boolean initialExists(String initialValue) {
		return asList(INITIALS).contains(initialValue);
	}

	static boolean variabilityExists(String variability) {
		return asList(VARIABILITIES).contains(variability);
	}

	static boolean startValueIsCorrect(String type, String value) {
		if (type == null) return false;
		switch (type) {
			case "Real":
				return isDouble(value);
			case "Integer":
				return isInteger(value);
			case "Boolean":
				return isBoolean(value);
			default:
				return true;
		}
	}

	private static boolean isDouble(String value) {
		try {
			parseDouble(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private static boolean isInteger(String value) {
		try {
			parseInteger(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private static boolean isBoolean(String value) {
		try {
			parseBoolean(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
