package fpd;

import io.intino.alexandria.inl.Message;
import sun.misc.IOUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class XmlCreator {
	private Message fmu;
	private List<Message> variables;
	private String varTemplate = "\t\t<ScalarVariable name=\"$$name$$\" valueReference=\"$$id$$\" causality=\"$$causality$$\" variability=\"$$variability$$\" initial=\"$$initial$$\">\n" +
			"\t\t\t<$$type$$ start=\"$$startValue$$\"/>\n" +
			"\t\t</ScalarVariable>";

	public XmlCreator(Message fmu, List<Message> variables) {
		this.fmu = fmu;
		this.variables = variables;
	}

	public void create(File projectFolder) {
		try {
			String modelDescriptionContent = new String(IOUtils.readFully(XmlCreator.class.getResourceAsStream("/modelDescription.xml"), -1, false));
			modelDescriptionContent = modelDescriptionContent.replace("$$name$$", fmu.get("name"));
			modelDescriptionContent = modelDescriptionContent.replace("$$date$$", Instant.now().toString());
			modelDescriptionContent = modelDescriptionContent.replace("$$variables$$", variables());
			Files.write(new File(projectFolder, "modelDescription.xml").toPath(), modelDescriptionContent.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String variables() {
		AtomicInteger valueReference = new AtomicInteger(0);
		return variables.stream().map(m -> {
			String varTemplate = this.varTemplate;
			varTemplate = varTemplate.replace("$$name$$", m.get("name"));
			varTemplate = varTemplate.replace("$$id$$", valueReference.addAndGet(1) + "");
			varTemplate = varTemplate.replace("$$causality$$", m.get("causality"));
			varTemplate = varTemplate.replace("$$variability$$", m.get("variability"));
			varTemplate = varTemplate.replace("$$type$$", m.get("type"));
			varTemplate = varTemplate.replace("$$initial$$", m.get("initial"));
			varTemplate = varTemplate.replace("$$startValue$$", m.get("startValue"));
			return varTemplate;
		}).collect(Collectors.joining("\n"));
	}
}
