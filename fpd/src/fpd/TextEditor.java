package fpd;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;

public class TextEditor extends JTextPane {

	private File file;
	private boolean modified;

	public TextEditor() throws IOException {
		this(null);
	}

	public TextEditor(File file) throws IOException {
		this.setFile(file);
		this.setFont(font());
		this.setBackground(new Color(0x27, 0x28, 0x22));
		this.setCaretColor(Color.white);
		this.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				modified = true;
				SwingUtilities.invokeLater(() -> rehighlight());
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				modified = true;
				SwingUtilities.invokeLater(() -> rehighlight());
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
			}
		});
	}

	private void rehighlight() {
		String[] lines = getText().split("\n");
		int pos = 0;
		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];
			if (line.startsWith("[") && line.endsWith("]")) {
				changeColorAt(pos, line.length(), new Color(0x91, 0xbf, 0xdb));
			} else if (line.contains(":")) {
				int index = line.indexOf(":");
				changeColorAt(pos, index + 1, new Color(0xfc, 0x8d, 0x59));
				changeColorAt(pos + index + 1, line.length() - index, Color.WHITE);
			} else {
				changeColorAt(pos, line.length(), Color.WHITE);
			}
			pos += line.length() + 1;
		}
	}

	private void changeColorAt(int pos, int length, Color color) {
		SimpleAttributeSet attrs = new SimpleAttributeSet();
		StyleConstants.setForeground(attrs, color);
		getStyledDocument().setCharacterAttributes(pos, length, attrs, true);
	}

	private void changeBackgroundColorAt(int pos, int length, Color color) {
		SimpleAttributeSet attrs = new SimpleAttributeSet();
		StyleConstants.setBackground(attrs, color);
		getStyledDocument().setCharacterAttributes(pos, length, attrs, false);
	}

	private String contentOf(File file) throws IOException {
		return file != null && file.exists() ?
				new String(Files.readAllBytes(file.toPath()), UTF_8) : template();
	}

	private Font font() {
		return new Font("Courier", Font.PLAIN, 16);
	}

	public void append(String content) {
		try {
			getDocument().insertString(getDocument().getLength(), content, null);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		SwingUtilities.invokeLater(this::rehighlight);
		modified = true;
	}

	void save() throws IOException {
		Files.write(file.toPath(), getText().getBytes(), TRUNCATE_EXISTING, CREATE);
		modified = false;
	}

	void save(File file) throws IOException {
		this.file = file;
		save();
	}

	public boolean isModified() {
		return modified;
	}

	public File file() {
		return file;
	}

	public void setFile(File file) throws IOException {
		this.file = file;
		this.setText(contentOf(file));
		this.setCaretPosition(getText().length());
		SwingUtilities.invokeLater(() -> {
			rehighlight();
			this.modified = false;
		});
	}

	private String template() {
		return "[FMU]" +
				"\nname: " + UUID.randomUUID().toString() +
				"\ndescription: Simulation model of SYSTEM_NAME system" +
				"\n\n";
	}

	void markAsError(int lineIndex) {
		String[] lines = getText().split("\n");
		int pos = 0;
		int index = 0;
		while (index < lineIndex) pos += lines[index++].length() + 1;
		changeColorAt(pos, lines[index].length(), Color.RED);
		setCaretPosition(pos);
	}

	public void changeLocation(File file) {
		this.file = file;
	}

	public int highlight(String textToFind) {
		rehighlight();
		int index = getText().indexOf(textToFind);
		while (index != -1) {
			changeBackgroundColorAt(index, textToFind.length(), new Color(0x21, 0x42, 0x83));
			index += textToFind.length();
			int findIndex = getText().substring(index).indexOf(textToFind);
			index = findIndex != -1 ? index + findIndex : -1;
		}
		return getText().indexOf(textToFind);
	}
}
