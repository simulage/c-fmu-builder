package fpd;

import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;

class Checker {
	private static Set<String> fmuAttributes = new HashSet<>(asList("name", "description"));
	private static Set<String> variableAttributes = new HashSet<>(asList("name", "type", "causality", "variability", "initial", "startValue"));
	private String currentTag = "FMU";
	private String currentType;
	private String[] lines;

	Checker() {
	}

	Error check(String text) {
		lines = text.split("\n");
		for (int lineIndex = 0; lineIndex < lines.length; lineIndex++) {
			String error = check(lineIndex);
			if (error == null) continue;
			return new Error(lineIndex, error);
		}
		return null;
	}

	private String check(int lineIndex) {
		String line = lines[lineIndex];
		if (lineIndex == 0) return checkFirstLine(line);
		else if (line.startsWith("[") && line.endsWith("]")) return checkTag(line);
		else if (line.contains(":")) return checkAttributeLine(line);
		else if (!line.trim().isEmpty()) return "Line must contain a <[Tag]> or an <attribute:>";
		return null;
	}

	private String checkFirstLine(String line) {
		return !line.equals("[FMU]") ? "Document must start by the first line [FMU]" : null;
	}

	private String checkTag(String line) {
		if (!line.equals("[Variable]")) return "Invalid tag " + line;
		currentTag = "Variable";
		return null;
	}

	private String checkAttributeLine(String line) {
		if (currentTag.equals("FMU")) return checkFMUAttributes(line);
		else if (currentTag.equals("Variable")) return checkVariableAttributes(line);
		return null;
	}

	private String attributeOf(String line) {
		return line.substring(0, line.indexOf(":")).trim();
	}

	private String checkFMUAttributes(String line) {
		return !fmuAttributes.contains(attributeOf(line)) ? "Attribute " + attributeOf(line) + " not known for tag [FMU]" : null;
	}

	private String checkVariableAttributes(String line) {
		String attribute = attributeOf(line);
		if (!variableAttributes.contains(attribute)) return "Attribute " + attribute + " not known for tag [Variable]";
		if (attribute.equals("type")) {
			if (!Variables.typeExists(valueOf(line))) return "Type value " + valueOf(line) + " not known";
			else currentType = valueOf(line);
		}
		if (attribute.equals("causality") && !Variables.causalityExists(valueOf(line)))
			return "Causality value " + valueOf(line) + " not known";
		if (attribute.equals("variability") && !Variables.variabilityExists(valueOf(line)))
			return "Variability value " + valueOf(line) + " not known";
		if (attribute.equals("initial") && !Variables.initialExists(valueOf(line)))
			return "Initial value " + valueOf(line) + " not known";
		if (attribute.equals("startValue") && !Variables.startValueIsCorrect(currentType, valueOf(line)))
			return "Start value " + valueOf(line) + " is not correct for type " + currentType;
		return null;
	}

	private String valueOf(String line) {
		return line.substring(line.indexOf(":") + 1).trim();
	}

}
