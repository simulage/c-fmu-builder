package fpd;

import javax.swing.*;
import java.awt.event.KeyEvent;

public class KeyListener implements java.awt.event.KeyListener {

	private Assigner assigner;

	public KeyListener(Assigner assigner) {
		this.assigner = assigner;
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {
		assigner.assign(((JTextField) e.getSource()).getText());
	}

	public interface Assigner {
		void assign(String text);
	}
}
