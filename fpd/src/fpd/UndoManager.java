package fpd;

import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.AbstractDocument;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.CompoundEdit;
import javax.swing.undo.UndoableEdit;
import java.time.Instant;
import java.util.ArrayList;

import static javax.swing.event.DocumentEvent.EventType.INSERT;
import static javax.swing.event.DocumentEvent.EventType.REMOVE;

class UndoManager extends AbstractUndoableEdit implements UndoableEditListener {
	private Instant instant = Instant.now();
	private ArrayList<Edit> edits = new ArrayList<>();
	private int pointer = -1;

	public UndoManager() {
	}

	public void undoableEditHappened(UndoableEditEvent e) {
		UndoableEdit edit = e.getEdit();
		if (edit instanceof AbstractDocument.DefaultDocumentEvent) {
			AbstractDocument.DefaultDocumentEvent event = (AbstractDocument.DefaultDocumentEvent) edit;
			if (!event.getType().equals(INSERT) && !event.getType().equals(REMOVE)) return;
			clearEdits();
			if (diffTime() > 500) {
				edits.add(new Edit());
				pointer++;
			}
			edits.get(pointer).addEdit(edit);
			instant = Instant.now();
		}
	}

	private void clearEdits() {
		while (pointer + 1 != edits.size()) edits.remove(edits.size() - 1);
	}

	private long diffTime() {
		return Instant.now().toEpochMilli() - instant.toEpochMilli();
	}

	public void undo() throws CannotUndoException {
		if (!canUndo()) throw new CannotUndoException();
		Edit u = edits.get(pointer);
		u.undo();
		pointer--;
	}

	public void redo() throws CannotUndoException {
		if (!canRedo()) throw new CannotUndoException();
		pointer++;
		Edit u = edits.get(pointer);
		u.redo();
	}

	public boolean canUndo() {
		return pointer >= 0;
	}

	public boolean canRedo() {
		return edits.size() > 0 && pointer < edits.size() - 1;
	}

	static class Edit extends CompoundEdit {
		boolean isUnDone = false;

		public void undo() throws CannotUndoException {
			super.undo();
			isUnDone = true;
		}

		public void redo() throws CannotUndoException {
			super.redo();
			isUnDone = false;
		}

		public boolean canUndo() {
			return edits.size() > 0 && !isUnDone;
		}

		public boolean canRedo() {
			return edits.size() > 0 && isUnDone;
		}

	}
}
