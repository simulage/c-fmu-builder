package fpd;

public class Error {
	private int lineIndex;
	private String error;

	public Error(int lineIndex, String error) {
		this.lineIndex = lineIndex;
		this.error = error;
	}

	public int lineIndex() {
		return lineIndex;
	}

	public String error() {
		return error;
	}
}
